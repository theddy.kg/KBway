package com.example.kbway.userRoute.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class ButtonData(
    val name: String
) : Parcelable